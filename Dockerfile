FROM python:3.9-slim

RUN apt-get update && apt-get upgrade -y && apt-get install libudev-dev libgl1 ffmpeg libsm6 libxext6 -y

WORKDIR /app

COPY requirements.txt ./

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

ENV FLASK_APP=app
ENV FLASK_ENV=production

CMD ["flask", "run", "--host=0.0.0.0"]

# FROM python:3.9-slim AS compile-image
# RUN apt-get update
# RUN apt-get install -y --no-install-recommends build-essential gcc

# RUN python -m venv /opt/venv
# # Make sure we use the virtualenv:
# ENV PATH="/opt/venv/bin:$PATH"

# COPY requirements.txt .
# RUN pip install -r requirements.txt

# COPY setup.py .
# COPY . .
# RUN pip install .

# FROM python:3.9-slim AS build-image
# COPY --from=compile-image /opt/venv /opt/venv

# # Make sure we use the virtualenv:
# ENV PATH="/opt/venv/bin:$PATH"
# CMD ['myapp']