import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    INFERENCE_SERVER_HOST = os.environ.get("INFERENCE_SERVER_HOST")
    INFERENCE_SERVER_PORT = os.environ.get("INFERENCE_SERVER_PORT")
    INFERENCE_SERVER_MODEL_NAME = os.environ.get("INFERENCE_SERVER_MODEL_NAME")

    LOG_LEVEL = os.environ.get("LOGLEVEL", "INFO").upper()
