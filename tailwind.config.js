/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./app/templates/**/*.{html,js}"],
    theme: {
        extend: {
            colors: {
                textColor: "#080f10",
                bgColor: "#f6fafa",
                primary: "#5eacae",
                secondary: "#a9b5d4",
                accentColor: "#787fbb",
            },
            fontWeight: {
                "extra-bold": "700",
            },
        },
    },
    plugins: [],
};
