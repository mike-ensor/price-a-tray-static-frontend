# Overview

This project is the frontend for Price a Tray game.

## Depenedencies

This project uses the APIs from Price a Tray Backend to manage the state of the Games, Players and Scores

## Run


## Dev Setup

1. Create a pyenv or venv/ 
2. `python -m pip install pip-tools`
3. `pip install -e .` 
4. `npm run style` 

