import logging

import requests
from app.main import bp
import json
from flask import (
    Response,
    jsonify,
    redirect,
    render_template,
    request,
    send_file,
)
from flask_cors import cross_origin

from config import Config

log = logging.getLogger(__name__)


# Home page
@bp.route("/", methods=["GET"])
def index():
    log.info("HOME PAGE")
    return render_template("index.html")


@bp.route("/favicon.ico")
def favicon():
    return send_file("static/favicon.ico", mimetype="image/vnd.microsoft.icon")
